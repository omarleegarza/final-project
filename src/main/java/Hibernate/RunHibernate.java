package Hibernate;
/* I added this code from my hibernate assignment to test connection */

import java.util.*;

public class RunHibernate {



    public static void main(String[] args) {


        EmployeeDAO b = EmployeeDAO.getInstance();

        List<Employee> c = b.getEmployees();
        for (Employee i : c) {
            System.out.println(i);
        }
    }

}
